#!/bin/sh
echo $0 $*
RA_DIR=/mnt/SDCARD/RetroArch
./cpufreq.sh
cd $RA_DIR/
HOME=$RA_DIR/ $RA_DIR/ra32.ss -v -L $RA_DIR/.retroarch/cores/snes9x2005_plus_libretro.so "$1"