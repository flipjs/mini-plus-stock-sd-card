#!/bin/sh
echo $0 $*
progdir=`dirname "$0"`
filename=${1##*/}
TMP1=${filename%.*}
if [[ "${TMP1#*.}" == "pia" ]]
then
 EMU="nestopia_libretro.so"
else
 EMU="fceumm_libretro.so"
fi

RA_DIR=/mnt/SDCARD/RetroArch
cd $RA_DIR/
HOME=$RA_DIR/ $RA_DIR/ra32.ss -v -L $RA_DIR/.retroarch/cores/$EMU "$1"